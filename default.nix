{ ocaml-ng, lib, opam2nix }:

let
  opam-selection = opam2nix.build {
    inherit (ocaml-ng.ocamlPackages_4_10) ocaml;
    selection = ./nix/opam-selection.nix;
    src = lib.cleanSource ./.;
  };
in opam-selection.hello
