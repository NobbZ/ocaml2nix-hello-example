### This file is generated by opam2nix.

self:
let
    lib = self.lib;
    opam-commit = "1461ccaad846db99d7bf16cf4c1b0ae9a05102b3";
    pkgs = self.pkgs;
    repo = (pkgs.fetchFromGitHub) 
    {
      owner = "ocaml";
      repo = "opam-repository";
      rev = opam-commit;
      sha256 = "189ycfz5h1p09rzxlzq7k4f2x9nvzpydv063i2ykbwdmyyydwigm";
    };
    repoPath = self.repoPath;
    selection = self.selection;
in
{
  format-version = 4;
  ocaml-version = "4.10.0";
  opam-commit = opam-commit;
  selection = 
  {
    base-bytes = 
    {
      opamInputs = 
      {
        ocaml = selection.ocaml;
        ocamlfind = selection.ocamlfind;
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:0a68lmbf68jgm1i3b59j2sc3ha9yhv4678f9mfwwvczw88prq7l3";
        package = "packages/base-bytes/base-bytes.base";
      };
      pname = "base-bytes";
      src = null;
      version = "base";
    };
    base-threads = 
    {
      opamInputs = {
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:1c4bpyh61ampjgk5yh3inrgcpf1z1xv0pshn54ycmpn4dyzv0p2x";
        package = "packages/base-threads/base-threads.base";
      };
      pname = "base-threads";
      src = null;
      version = "base";
    };
    base-unix = 
    {
      opamInputs = {
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:0mpsvb7684g723ylngryh15aqxg3blb7hgmq2fsqjyppr36iyzwg";
        package = "packages/base-unix/base-unix.base";
      };
      pname = "base-unix";
      src = null;
      version = "base";
    };
    conf-m4 = 
    {
      buildInputs = [ (pkgs.m4) ];
      opamInputs = {
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:1jlhg718lz35jyr5w0sgvg5ycplhnd8653rc4980yci8p3z1vlxs";
        package = "packages/conf-m4/conf-m4.1";
      };
      pname = "conf-m4";
      src = null;
      version = "1";
    };
    cppo = 
    {
      opamInputs = 
      {
        base-unix = selection.base-unix;
        dune = selection.dune;
        ocaml = selection.ocaml;
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:0hdl429cpb4bg9gc07rxs14p7d3r3nfi3vw6s38c6xhf412nl611";
        package = "packages/cppo/cppo.1.6.6";
      };
      pname = "cppo";
      src = pkgs.fetchurl 
      {
        sha256 = "185q0x54id7pfc6rkbjscav8sjkrg78fz65rgfw7b4bqlyb2j9z7";
        url = "https://github.com/ocaml-community/cppo/releases/download/v1.6.6/cppo-v1.6.6.tbz";
      };
      version = "1.6.6";
    };
    dune = 
    {
      opamInputs = 
      {
        base-threads = selection.base-threads;
        base-unix = selection.base-unix;
        ocaml = selection.ocaml or null;
        ocamlfind-secondary = selection.ocamlfind-secondary or null;
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:0h4rn3lr4jjyjlhrj8vih4bjvw7b4q4hlhj0q020h3l4fpvfdbf6";
        package = "packages/dune/dune.2.6.0";
      };
      pname = "dune";
      src = pkgs.fetchurl 
      {
        sha256 = "1hvgj78xqqqph8dwn1jjkjp8bpppvwzx33lzkvwh5wn5zd4xij8j";
        url = "https://github.com/ocaml/dune/releases/download/2.6.0/dune-2.6.0.tbz";
      };
      version = "2.6.0";
    };
    dune-configurator = 
    {
      opamInputs = 
      {
        dune = selection.dune;
        dune-private-libs = selection.dune-private-libs;
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:1hjh87qwacygdm0mqai22qcqw7c3bl5q9qdafgcxm0gh3j4a4az0";
        package = "packages/dune-configurator/dune-configurator.2.6.0";
      };
      pname = "dune-configurator";
      src = pkgs.fetchurl 
      {
        sha256 = "1hvgj78xqqqph8dwn1jjkjp8bpppvwzx33lzkvwh5wn5zd4xij8j";
        url = "https://github.com/ocaml/dune/releases/download/2.6.0/dune-2.6.0.tbz";
      };
      version = "2.6.0";
    };
    dune-private-libs = 
    {
      opamInputs = 
      {
        dune = selection.dune;
        ocaml = selection.ocaml;
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:09yh3hkld97hfdsqz38i384mr2vsjby33c5pk286749hg6arjvm3";
        package = "packages/dune-private-libs/dune-private-libs.2.6.0";
      };
      pname = "dune-private-libs";
      src = pkgs.fetchurl 
      {
        sha256 = "1hvgj78xqqqph8dwn1jjkjp8bpppvwzx33lzkvwh5wn5zd4xij8j";
        url = "https://github.com/ocaml/dune/releases/download/2.6.0/dune-2.6.0.tbz";
      };
      version = "2.6.0";
    };
    hello = 
    {
      opamInputs = 
      {
        dune = selection.dune;
        lwt = selection.lwt;
      };
      opamSrc = "hello.opam";
      pname = "hello";
      src = self.directSrc "hello";
      version = "development";
    };
    lwt = 
    {
      opamInputs = 
      {
        base-threads = selection.base-threads or null;
        base-unix = selection.base-unix or null;
        conf-libev = selection.conf-libev or null;
        cppo = selection.cppo;
        dune = selection.dune;
        dune-configurator = selection.dune-configurator;
        mmap = selection.mmap;
        ocaml = selection.ocaml;
        ocaml-syntax-shims = selection.ocaml-syntax-shims or null;
        ocplib-endian = selection.ocplib-endian;
        result = selection.result;
        seq = selection.seq;
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:1b83y1ccdk4f38961sf0bfawxyb6r0gc88bvz8xzh5hb88s52hbm";
        package = "packages/lwt/lwt.5.3.0";
      };
      pname = "lwt";
      src = pkgs.fetchurl 
      {
        sha256 = "1barwvlyqd3xiqhqr01yrcygjvn26yz0rdh68rgnhyx0g21r5kiq";
        url = "https://github.com/ocsigen/lwt/archive/5.3.0.tar.gz";
      };
      version = "5.3.0";
    };
    mmap = 
    {
      opamInputs = 
      {
        dune = selection.dune;
        ocaml = selection.ocaml;
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:0mjn2jxrfwlrzppjnc6pg47ncm0ri6a1hn3kj2yz10rmpca25bsm";
        package = "packages/mmap/mmap.1.1.0";
      };
      pname = "mmap";
      src = pkgs.fetchurl 
      {
        sha256 = "0l6waidal2n8mkdn74avbslvc10sf49f5d889n838z03pra5chsc";
        url = "https://github.com/mirage/mmap/releases/download/v1.1.0/mmap-v1.1.0.tbz";
      };
      version = "1.1.0";
    };
    ocaml = 
    {
      opamInputs = 
      {
        ocaml-base-compiler = selection.ocaml-base-compiler or null;
        ocaml-config = selection.ocaml-config;
        ocaml-system = selection.ocaml-system or null;
        ocaml-variants = selection.ocaml-variants or null;
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:1j9xgxnbgzrar4rwynm7jd0bi3f5qwwkgyxvk1pd8iazvn81pgya";
        package = "packages/ocaml/ocaml.4.10.0";
      };
      pname = "ocaml";
      src = null;
      version = "4.10.0";
    };
    ocaml-base-compiler = 
    {
      opamInputs = {
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:0wavwn6cq999v787fsxf0v2z71h1vwhxwqbidznc4f9ccwjcdc76";
        package = "packages/ocaml-base-compiler/ocaml-base-compiler.4.10.0";
      };
      pname = "ocaml-base-compiler";
      src = pkgs.fetchurl 
      {
        sha256 = "0fdw4abyp37q7acqaqawy64gakpg7xckw5ssfpn8dbwxlzqf1fjq";
        url = "https://github.com/ocaml/ocaml/archive/4.10.0.tar.gz";
      };
      version = "4.10.0";
    };
    ocaml-config = 
    {
      opamInputs = 
      {
        ocaml-base-compiler = selection.ocaml-base-compiler or null;
        ocaml-system = selection.ocaml-system or null;
        ocaml-variants = selection.ocaml-variants or null;
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:0g5s0yysgqdrbgx7vyh56fhx59xypw6hdwlcbzbqcgvj4zp4yy0c";
        package = "packages/ocaml-config/ocaml-config.1";
      };
      pname = "ocaml-config";
      src = null;
      version = "1";
    };
    ocamlfind = 
    {
      opamInputs = 
      {
        conf-m4 = selection.conf-m4;
        graphics = selection.graphics or null;
        ocaml = selection.ocaml;
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:04z3rq1y20wfzmwvjm9wlg89cqqs8n37inhbwp4x2dsqbn0hqd81";
        package = "packages/ocamlfind/ocamlfind.1.8.1";
      };
      pname = "ocamlfind";
      src = pkgs.fetchurl 
      {
        sha256 = "00s3sfb02pnjmkax25pcnljcnhcggiliccfz69a72ic7gsjwz1cf";
        url = "http://download.camlcity.org/download/findlib-1.8.1.tar.gz";
      };
      version = "1.8.1";
    };
    ocplib-endian = 
    {
      opamInputs = 
      {
        base-bytes = selection.base-bytes;
        cppo = selection.cppo;
        dune = selection.dune;
        ocaml = selection.ocaml;
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:0j5ymxa4ky208k8m4bprvmvbqf9jnr4d0xwdaivznbv85jq9hw4q";
        package = "packages/ocplib-endian/ocplib-endian.1.1";
      };
      pname = "ocplib-endian";
      src = pkgs.fetchurl 
      {
        sha256 = "0qy5q7p11gxi5anhvi8jj6mr80ml0ih8lax5k579rsr2hsp3sns5";
        url = "https://github.com/OCamlPro/ocplib-endian/archive/1.1.tar.gz";
      };
      version = "1.1";
    };
    result = 
    {
      opamInputs = 
      {
        dune = selection.dune;
        ocaml = selection.ocaml;
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:1c7lw8dbchllz3rl801xwpm82r427vnrv7b7kqh0gwjglya50y28";
        package = "packages/result/result.1.5";
      };
      pname = "result";
      src = pkgs.fetchurl 
      {
        sha256 = "0cpfp35fdwnv3p30a06wd0py3805qxmq3jmcynjc3x2qhlimwfkw";
        url = "https://github.com/janestreet/result/releases/download/1.5/result-1.5.tbz";
      };
      version = "1.5";
    };
    seq = 
    {
      opamInputs = {
                     ocaml = selection.ocaml;
      };
      opamSrc = repoPath repo 
      {
        hash = "sha256:1vm8mk6zm2q3fwnkprl6jib048zr4ysldw0bl74x6wwbxj0vx6k9";
        package = "packages/seq/seq.base";
      };
      pname = "seq";
      src = null;
      version = "base";
    };
  };
}

