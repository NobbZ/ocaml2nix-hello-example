let
  sources = import ./nix/sources.nix {};
  nixpkgs = import sources.nixpkgs {};
  opam2nix = import sources.opam2nix {};

  inherit (nixpkgs) mkShell callPackage;

  hello = callPackage ./. { inherit opam2nix; };
in mkShell {
  name = "ocaml-hello-opam2nix";

  buildInputs = [ opam2nix hello ];
}
